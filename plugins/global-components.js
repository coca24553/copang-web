import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CardShoesInfo from '~/components/card-shoes-info'
Vue.component('CardShoesInfo', CardShoesInfo)
